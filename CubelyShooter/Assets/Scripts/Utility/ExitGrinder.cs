﻿using UnityEngine;
using System.Collections;

public class ExitGrinder : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<PlayerBulletScript>())
		{
			other.GetComponent<PlayerBulletScript>().Destroy();
			Debug.Log("Exit");
		}
	}
}
