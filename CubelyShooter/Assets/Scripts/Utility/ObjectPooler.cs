﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

	//public static ObjectPooler instance = null;
	public GameObject m_PooledObject;
	public int m_PooledAmount = 10;
	public bool m_WillGrow;

	List<GameObject> m_PooledObjects;

	void Start()
	{
		m_PooledObjects = new List<GameObject>();

		for(int i = 0; i < m_PooledAmount; i++)
		{
			GameObject obj = (GameObject)Instantiate(m_PooledObject);
			obj.transform.parent = transform;
			obj.SetActive(false);
			m_PooledObjects.Add(obj);
		}
	}


	public GameObject GetPooledObject()
	{
		for(int i = 0; i < m_PooledObjects.Count; i++)
		{
			if(!m_PooledObjects[i].activeInHierarchy)
			{
				return m_PooledObjects[i];
			}
		}

		if(m_WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(m_PooledObject);
			m_PooledObjects.Add(obj);
			obj.transform.parent = transform;
			obj.SetActive(false);
			return obj;
		}

		return null;
	}
}
