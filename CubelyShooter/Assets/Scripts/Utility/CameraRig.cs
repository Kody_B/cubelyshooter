﻿using UnityEngine;
using System.Collections;

public class CameraRig : MonoBehaviour {

	[SerializeField] float m_DampTime;

	Transform player;
	Vector3 m_MoveVelocity;

	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 camToPlayer = new Vector3(player.transform.position.x, 0, player.transform.position.z);
		transform.position =  Vector3.SmoothDamp(transform.position,camToPlayer, ref m_MoveVelocity, m_DampTime);
	}
}
