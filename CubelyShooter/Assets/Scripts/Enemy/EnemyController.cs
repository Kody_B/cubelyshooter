﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	Transform m_Player;

	private NavMeshAgent m_NavMeshAgent;

	void Start()
	{
		m_NavMeshAgent = GetComponent<NavMeshAgent>();
		m_Player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update () 
	{
		
		m_NavMeshAgent.SetDestination(m_Player.position);
	}
}
