using System;
using UnityEngine;


[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof (CapsuleCollider))]
public class RigidbodyPlayerMovement : MonoBehaviour
{
    [Serializable]
    public class MovementSettings
    {
        public float m_Speed = 8.0f;
        public float JumpForce = 30f;
		public float m_TurnSpeed = 500;
        [HideInInspector] public float CurrentTargetSpeed = 8f;

        public void UpdateDesiredTargetSpeed(Vector2 input)
        {
            if (input == Vector2.zero) return;

			CurrentTargetSpeed = m_Speed;
        }
    }


    [Serializable]
    public class AdvancedSettings
    {
        public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
        public float stickToGroundHelperDistance = 0.5f; // stops the character
        [Tooltip("set it to 0.1 or more if you get stuck in wall")]
        public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
		public float m_CamRayLength;
    }

    public MovementSettings movementSettings = new MovementSettings();
    public AdvancedSettings advancedSettings = new AdvancedSettings();


    private Rigidbody m_RigidBody;
    private CapsuleCollider m_Capsule;
    private float m_YRotation;
	private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded;
	private Vector2 m_Input;

	private Crosshair m_Crosshair;

    public Vector3 Velocity
    {
        get { return m_RigidBody.velocity; }
    }

    public bool Grounded
    {
        get { return m_IsGrounded; }
    }

    public bool Jumping
    {
        get { return m_Jumping; }
    }

    private void Start()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();

		m_Crosshair = GameObject.FindObjectOfType<Crosshair>();
    }


    private void Update()
    {

		if (Input.GetButtonDown("Jump") && !m_Jump)
        {
			
            m_Jump = true;
        }

		if(GetInput() == Vector2.zero)
		{
			m_RigidBody.velocity = new Vector3(0,m_RigidBody.velocity.y, 0);
		}
		else
		{
			m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, m_RigidBody.velocity.y, m_RigidBody.velocity.z);
		}

		m_Input = GetInput();

    }


    private void FixedUpdate()
    {
		
        GroundCheck();

		Move();

		Jump();

		Turn();
    }

	private void Jump()
	{
		if (m_IsGrounded)
		{
			m_RigidBody.drag = 5f;

			if (m_Jump)
			{
				m_RigidBody.drag = 0f;
				m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, 0f, m_RigidBody.velocity.z);
				m_RigidBody.AddForce(new Vector3(0f, movementSettings.JumpForce, 0f), ForceMode.Impulse);
				m_Jumping = true;
			}

			if (!m_Jumping && Mathf.Abs(m_Input.x) < float.Epsilon && Mathf.Abs(m_Input.y) < float.Epsilon && m_RigidBody.velocity.magnitude < 1f)
			{
				m_RigidBody.Sleep();
			}
		}
		else
		{
			m_RigidBody.drag = 0f;
			if (m_PreviouslyGrounded && !m_Jumping)
			{
				StickToGroundHelper();
			}
		}
		m_Jump = false;
	}

	private void Move()
	{
		if ((Mathf.Abs(m_Input.x) > float.Epsilon || Mathf.Abs(m_Input.y) > float.Epsilon))
		{
			// always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = Vector3.forward*m_Input.y + Vector3.right*m_Input.x;

			desiredMove.x = desiredMove.x*movementSettings.CurrentTargetSpeed;
			desiredMove.z = desiredMove.z*movementSettings.CurrentTargetSpeed;
			desiredMove.y = desiredMove.y*movementSettings.CurrentTargetSpeed;

			m_RigidBody.velocity = new Vector3(desiredMove.x, m_RigidBody.velocity.y, desiredMove.z);

		}
	}

	private void Turn()
	{
		Vector3 playerToMouse = m_Crosshair.GetLookPosition() - transform.position;
		
		playerToMouse.y = 0;

		if(playerToMouse != Vector3.zero)
		{
			Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

			m_RigidBody.rotation = Quaternion.RotateTowards(m_RigidBody.rotation, newRotation, movementSettings.m_TurnSpeed * Time.deltaTime);
		}

	}

    private void StickToGroundHelper()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                               ((m_Capsule.height/2f) - m_Capsule.radius) +
                               advancedSettings.stickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
        {
            if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
            {
                m_RigidBody.velocity = Vector3.ProjectOnPlane(m_RigidBody.velocity, hitInfo.normal);
            }
        }
    }


    private Vector2 GetInput()
    {
        
        Vector2 input = new Vector2
            {
                x = Input.GetAxis("Horizontal"),
                y = Input.GetAxis("Vertical")
            };
		movementSettings.UpdateDesiredTargetSpeed(input);
        return input;
    }

    /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
    private void GroundCheck()
    {
        m_PreviouslyGrounded = m_IsGrounded;
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                               ((m_Capsule.height/2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
        {
            m_IsGrounded = true;
        }
        else
        {
            m_IsGrounded = false;
        }
        if (!m_PreviouslyGrounded && m_IsGrounded && m_Jumping)
        {
            m_Jumping = false;
        }
    }
}

