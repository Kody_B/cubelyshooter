﻿using UnityEngine;
using System.Collections;

public class PlayerWeaponScript : MonoBehaviour {

	[SerializeField] GameObject m_PooledObjectsGameObject;

	[SerializeField] float m_FireRate;
	private float m_NextFire;

	public float m_HeightOffset;

	ObjectPooler m_PooledObjects;
	Crosshair m_CrossHair;

	void Awake()
	{
		m_PooledObjectsGameObject = Instantiate(m_PooledObjectsGameObject);
		m_PooledObjectsGameObject.transform.parent = transform.parent.parent;

		m_PooledObjects = m_PooledObjectsGameObject.GetComponent<ObjectPooler>();
		m_CrossHair = GameObject.Find("PlayerCrosshair").GetComponent<Crosshair>();
	}

	void Update()
	{

		if(Input.GetButton("Fire1") && Time.time > m_NextFire)
		{
			m_NextFire = Time.time + m_FireRate;
			Fire();
		}

	}

	void FixedUpdate()
	{
		
		Vector3 crossHairUp = new Vector3(m_CrossHair.GetLookPosition().x, m_CrossHair.GetLookPosition().y + m_HeightOffset, m_CrossHair.GetLookPosition().z);
		transform.LookAt(crossHairUp);

	}
	
	public void Fire()
	{
		GameObject bullet = m_PooledObjects.GetPooledObject();

		if(bullet == null)
		{
			return;
		}

		bullet.transform.position = transform.GetChild(0).position;
		bullet.transform.rotation = transform.GetChild(0).rotation;
		bullet.SetActive(true);
	}
}
