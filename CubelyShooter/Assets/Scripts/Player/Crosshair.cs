﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

	public LayerMask m_RaycastMask;

	Vector3 m_CrossHairPosition;

	void FixedUpdate () 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if(Physics.Raycast(ray, out hit, 100, m_RaycastMask))
		{
			m_CrossHairPosition = hit.point;

			transform.position = m_CrossHairPosition;

		}
	}

	public Vector3 GetLookPosition()
	{
		return m_CrossHairPosition;
	}

}
