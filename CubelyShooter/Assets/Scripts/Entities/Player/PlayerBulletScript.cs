﻿using UnityEngine;
using System.Collections;

public class PlayerBulletScript : MonoBehaviour {


	public float m_Speed = 5f;

	void OnEnable()
	{
		//Invoke("Destroy", 2f);
	}

	void FixedUpdate () 
	{
		transform.Translate(0,0, m_Speed * Time.deltaTime);
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player") || other.CompareTag("PlayerBullet"))
		{
			return;
		}

		if(other.CompareTag("Enemy"))
		{
			Debug.Log("Enemy hit");
		}

		Destroy();
	}

	public void Destroy()
	{
		gameObject.SetActive(false);
	}
}
